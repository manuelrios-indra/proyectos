package com.telefonica.pe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovistarStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovistarStoreApplication.class, args);
	}

}
