package com.telefonica.pe.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.telefonica.pe.entity.Store;
import com.telefonica.pe.services.IStoreService;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "stores")
@RestController
public class StoreController {

	@Autowired
	private IStoreService storeService;

	
	@ApiOperation(value = "Obtiene la lista de las tiendas", nickname = "listartiendas", notes = "",  tags = {
			    "listar", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Lista de suscriptores", response = Store.class, responseContainer  = "List") })	 
	@GetMapping("/listarTiendas")
	public List<Store> listarTiendas(){
		return storeService.listarTiendas();
	}
	
	@ApiOperation(value = "registro de tiendas", nickname = "grabar", notes = "",  tags = {
		    "grabar", })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "registro de tienda", response = Store.class) })	 
	@PostMapping("/grabar")
	public Store grabar( @NotNull @ApiParam(value = "Tienda", required = true) @RequestBody Store store) {
		return storeService.registrarTienda(store);
	}
	
	
	
}
