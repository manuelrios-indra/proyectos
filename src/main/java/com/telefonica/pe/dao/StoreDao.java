package com.telefonica.pe.dao;

import org.springframework.data.repository.CrudRepository;

import com.telefonica.pe.entity.Store;

public interface StoreDao extends CrudRepository<Store, String>{

}
