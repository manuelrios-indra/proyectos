package com.telefonica.pe.services;

import java.util.List;

import com.telefonica.pe.entity.Store;


public interface IStoreService {
	
	public List<Store> listarTiendas();
	public Store registrarTienda(Store s);
	
}
