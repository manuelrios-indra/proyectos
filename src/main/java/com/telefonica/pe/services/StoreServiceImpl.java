package com.telefonica.pe.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.pe.dao.StoreDao;
import com.telefonica.pe.entity.Store;

@Service
public class StoreServiceImpl implements IStoreService{

	@Autowired
	private StoreDao storeDao;
	
	@Override
	public List<Store> listarTiendas() {
		
		return (List<Store>)storeDao.findAll();
	}

	@Override
	public Store registrarTienda(Store s) {
		return storeDao.save(s);
	}

}
